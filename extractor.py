""" Log into goodreads and download your library """
import os
import time
from pathlib import Path
import logging

import configargparse
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import geckodriver_autoinstaller


def get_driver(download_path=".", driver_dir="."):
    logging.info("Creating a browser to download our files")
    """Initialize the driver with a special download friendly profile"""
    # firefox in Ubuntu 22.04+ is provided via snap
    # snap has very strict containment and can't access the /tmp directory
    # So we will redefine that to be local to the download_dir where we know we
    # already have write ability (or should!)
    os.environ["TMPDIR"] = download_path
    # set up a special download friendly firefox profile
    profile = webdriver.FirefoxProfile()
    # do not use the default download directory
    profile.set_preference("browser.download.folderList", 2)
    # turn off showing the download progress
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    # set the download directory to where we want it
    profile.set_preference("browser.download.dir", download_path)
    # automatically download files like this instead of asking
    profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/csv")
    profile.update_preferences()

    # let's be headless
    options = Options()
    options.headless = True
    options.profile = profile
    # start up selenium
    return webdriver.Firefox(options=options)


def login(browser, username, password):
    """Logs into the session and exits"""
    logging.info("Logging into the website")
    # original sign_in now just has a link to another page for signing in
    browser.get(
        "https://www.goodreads.com/ap/signin?language=en_US&openid.assoc_handle=amzn_goodreads_web_na&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.goodreads.com%2Fap-handler%2Fsign-in"
    )
    browser.find_element_by_id("ap_email").send_keys(username)
    browser.find_element_by_id("ap_password").send_keys(password)
    browser.find_element_by_id("signInSubmit").click()


def export_booklist(browser, timeout):
    """export all books from your account"""
    logging.info("Requesting export")
    # now that we are logged in, let's export our books
    browser.get("https://www.goodreads.com/review/import")
    elem = browser.find_element_by_xpath("//*[text()='Export Library']")
    elem.click()
    logging.info("waiting for export and downloading")
    # wait for the export
    browser.implicitly_wait(timeout)
    export_link = browser.find_element_by_partial_link_text("Your export from")
    export_link.click()


if __name__ == "__main__":
    curdir = str(Path(__file__).parent.absolute())
    timeout_default = 120
    p = configargparse.ArgParser(default_config_files=["~/.config/export_booklist.ini"])
    # TODO add a dated directory argument so we can have predictable file paths
    # otherwise you get silly overwriting issues
    p.add(
        "--download_dir",
        default=curdir,
        help=f"Path to download the library csv file. Defaults to {curdir}",
    )
    p.add("--username", help="Username for logging into goodreads")
    p.add(
        "--password",
        help="Password for logging into goodreads. People can read this value if you pass it as an argument into a process on your system. Better to save it to a config file that only your user can read",
    )
    p.add(
        "--timeout",
        default=timeout_default,
        help=f"How long to wait for the export before giving up. Defaults to {timeout_default} seconds",
    )
    args = p.parse_args()
    try:
        geckodriver_autoinstaller.install()
    except TimeoutError as e:
        logging.warn(
            "Could not auto install or update gecko driver - unless this is the first run, it is safe to ignore this error as we can function without updating this."
        )
    driver = get_driver(download_path=args.download_dir, driver_dir=curdir)
    try:
        login(driver, args.username, args.password)
        export_booklist(driver, args.timeout)
        # sleep a few seconds to give the download time to complete
        time.sleep(30)
    finally:
        driver.quit()
