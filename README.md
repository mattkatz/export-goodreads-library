# Auto export a goodreads library

The goal is to have a cron job able to once a week or so get me my good reads library export locally so that I can back it up and do things with it.


## plan

Let's follow https://swethatanamala.github.io/2018/09/01/web-scraping-using-python-selenium-and-beautiful-soup/ and see how it goes

## installation

clone this repo

    python3 -m venv env
    . env/bin/activate
    pip install -r requirements.txt

On first running, the script will download the latest [geckodriver](https://github.com/mozilla/geckodriver/releases) and put it in your $PATH somewhere.

You will want to store your username and password somewhat securely.

    mkdir -p ~/.config
    touch ~/.config/export_booklist.ini

Now edit the `~/.config/export_booklist.ini` file to include 

    username=<YOUR GOODREADS EMAIL>
    password=<YOUR_GOODREADS_PASSWORD>

How I run this:
I like to keep logs in a .logs directory:

    mkdir -p ~/.logs/bookexport

I keep environments and run scripts for my cron jobs in a special directory.

    mkdir -p ~/crons/booklists/
    cd ~/crons/booklists
    python3 -m venv env
    ~/crons/booklists/env/bin/pip install -r <PATH_TO_THIS_REPO>/requirements.txt
    cp <PATH_TO_THIS_REPO>/run.sh .

I create a cron tab entry like so:

    0 5 * * * ~/crons/booklists/run.sh >> ~/.logs/bookexport/`date +"\%F"`-run.log 2>&1




## usage

You should now be able to cron a run of this script from that python environment to execute and download your goodreads library csv

use the --downloads_dir argument to choose where the csv should go

## troubleshooting

### Firefox profile is missing or can't be opened
I encountered this because ubuntu 21.10 and up ship firefox as a snap package that has a tighter security model than a typical apt install. It's been suggested to remove the snap package and then install it as an apt

    sudo snap remove firefox
    cd ~/snap
    rm -r firefox

    sudo apt-get install firefox

This worked for me.
