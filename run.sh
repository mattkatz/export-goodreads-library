#! /bin/bash

set -euo pipefail
# enable to see all output
# set x

# checkout the most recent version of the script

TMPDIR=$(mktemp -d)

cleanup(){
  rm -rf "$TMPDIR";
}

function notify(){
  echo "something went wrong here!"
  echo "$(caller): ${BASH_COMMAND}"
  echo "You may want to re-run with set x enabled"
  notify-send "Export Goodreads failed" "Check the logfiles at ~/.logs/bookexport/"
}

trap notify ERR
trap cleanup EXIT

cd $TMPDIR
git clone git@gitlab.com:mattkatz/export-goodreads-library.git . > /dev/null 2>&1;

~/crons/booklists/env/bin/python extractor.py --download_dir ~/Documents/booklists
